package com.deveducation.personsrest.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    private static final Person EMPTY = new Person();

    @NotNull(message = "ID is Required")
    private int id;

    @NotBlank(message = "First Name is required")
    private String fName;

    @NotBlank(message = "Last name is required")
    private String lName;

    @Max(value = 100, message = "Max limit")
    private int age;

    public boolean isEmpty() {
        return this.equals(EMPTY);
    }

}