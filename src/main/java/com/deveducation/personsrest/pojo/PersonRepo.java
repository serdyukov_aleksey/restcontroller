package com.deveducation.personsrest.pojo;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PersonRepo {

    private static List<Person> people = new ArrayList<>();

    static {
        people.add(new Person(1, "Ivanov", "Ivan", 32));
        people.add(new Person(2, "Grigoriy", "Grigoriev", 33));
        people.add(new Person(3, "Serggev", "Sergey", 42));
        people.add(new Person(4, "Dmitriy", "Dmitriev", 22));
        people.add(new Person(5, "Efrosiniya", "Kozlova", 45));
    }

    public List<Person> getAll() {
        return people;
    }

    public List<Person> getById(int id) {
        return people.stream()
                .filter(e -> e.getId() == id)
                .collect(Collectors.toList());
    }

    public void addPerson(Person person) {
        people.add(person);
    }

    public void updatePerson(Person person) {
        people.stream()
                .filter(e -> e.getId() == person.getId())
                .forEach(e -> {
                    e.setAge(person.getAge());
                    e.setLName(person.getLName());
                    e.setFName(person.getFName());
                });
    }

    public void deletePerson(int id) {
        List<Person> arr = people
                .stream()
                .filter(e -> e.getId() == id)
                .collect(Collectors.toList());
        people.removeAll(arr);
    }

}