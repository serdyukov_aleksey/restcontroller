package com.deveducation.personsrest.controllers;

import com.deveducation.personsrest.pojo.PersonRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import com.deveducation.personsrest.pojo.Person;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/person")
@RequiredArgsConstructor
public class PersonController {
    private final PersonRepo personRepo;

    @GetMapping(value = {"/all"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Person> getAllPersons() {
        return personRepo.getAll();
    }

    @GetMapping(value = {"/id/{id}"}, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Person> getPersonById(@PathVariable(name = "id") int id) {
        return personRepo.getById(id);
    }

    @PostMapping(path = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public void savePerson(@Valid @RequestBody Person person) {
        personRepo.addPerson(person);
    }

    @PutMapping(path = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public void update(@RequestBody Person person) {
        personRepo.updatePerson(person);
    }

    @DeleteMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deletePerson(@PathVariable int id) {
        personRepo.deletePerson(id);
    }
}