package com.deveducation.personsrest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Wrong params")
public class NotFound extends RuntimeException {
    public NotFound(String message) {
        super(message);
    }
}
